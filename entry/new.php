<?php
session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once '../classes/HtmlMessageWriter.php';

require_once '../classes/HtmlEntryWriter.php';


/*
 * html output
 */
require_once '../parts/header.php';
echo HtmlMessageWriter::writeMessage($message);
echo HtmlEntryWriter::writeCreateForm();
require_once '../parts/footer.php';