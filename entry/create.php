<?php
session_start();

if (empty($_POST['title'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to add new entry. Required fields must be specified'
    ];
    header('Location: /entry/new.php');
    die();
}

require_once '../classes/Core.php';
require_once '../classes/Entry.php';

$entry = new Entry($_POST['title'], $_POST['intro'], $_POST['content']);
$entry->create();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'New entry has been added successfully'
];
header('Location: /index.php');