<?php
if (empty($_GET['id'])) {
    header('Location: /index.php');
    die();
}

session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once '../classes/HtmlMessageWriter.php';

require_once '../classes/Core.php';
require_once '../classes/Entry.php';
require_once '../classes/HtmlEntryWriter.php';

require_once '../classes/Comment.php';
require_once '../classes/HtmlCommentWriter.php';

$entry = new Entry();
$html = HtmlEntryWriter::writeDetails($entry->getById($_GET['id']));

$comment = new Comment();
$html .= HtmlCommentWriter::writeList($comment->getAll($_GET['id']));
$html .= HtmlMessageWriter::writeMessage($message);
$html .= HtmlCommentWriter::writeCreateForm($_GET['id']);


/*
 * html output
 */
require_once '../parts/header.php';
echo $html;
require_once '../parts/footer.php';