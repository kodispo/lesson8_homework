<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

if (empty($_POST['title'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to update entry information. Required fields must be specified'
    ];
    header('Location: /entry/edit.php?id=' . $_POST['id']);
    die();
}

require_once '../classes/Core.php';
require_once '../classes/Entry.php';

$entry = new Entry($_POST['title'], $_POST['intro'], $_POST['content']);
$entry->setId($_POST['id']);
$entry->update();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Entry information has been updated successfully'
];
header('Location: /entry/edit.php?id=' . $_POST['id']);