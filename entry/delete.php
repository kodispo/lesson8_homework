<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Core.php';
require_once '../classes/Entry.php';

$entry = new Entry();
$entry->setId($_POST['id']);
$entry->delete();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Entry has been removed successfully'
];
header('Location: /index.php');