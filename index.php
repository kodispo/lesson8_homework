<?php
ini_set('display_errors', 'On');

session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once 'classes/HtmlMessageWriter.php';

require_once 'classes/Core.php';
require_once 'classes/Db.php';
require_once 'classes/Entry.php';
require_once 'classes/HtmlEntryWriter.php';

$db = new Db();
$entries = new Entry();


/*
 * html output
 */
require_once 'parts/header.php';
echo HtmlMessageWriter::writeMessage($message);
require_once 'parts/dbControl.php';
if ($db->hasEntriesTable() && $db->hasCommentsTable()) {
    echo HtmlEntryWriter::writeTable($entries->getAll());
}
require_once 'parts/footer.php';