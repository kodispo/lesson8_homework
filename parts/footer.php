
                <?php if ($_SERVER['DOCUMENT_URI'] != '/index.php') : ?>
                    <div class="my-5">
                        <a href="/index.php" class="btn btn-outline-primary btn-sm">Back</a>
                    </div>
                <?php endif; ?>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>