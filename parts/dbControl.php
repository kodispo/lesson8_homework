<div class="mb-5">
    <a href="../db/create_db.php" class="btn btn-outline-primary">Create tables</a>
    <a href="../db/seeder_db.php" class="btn btn-outline-primary">Populate tables</a>
    <a href="../db/remove_db.php" class="btn btn-outline-danger">Remove tables</a>
</div>