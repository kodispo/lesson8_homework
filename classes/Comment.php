<?php
class Comment extends Core {
    
    protected $id;
    protected $name;
    protected $body;
    protected $entry_id;
    
    public function __construct($name = '', $body = '', $entry_id = 0) {
        parent::__construct();
        
        $this->name = htmlspecialchars($name);
        $this->body = htmlspecialchars($body);
        $this->entry_id = (int) htmlspecialchars($entry_id);
    }
    
    public function getAll($entry_id) {
        $pdo = $this->getPdo();
        $entry_id = (int) htmlspecialchars($entry_id);
    
        try {
        
            $sql = 'SELECT name, body FROM comments WHERE entry_id = :entry_id';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':entry_id', $entry_id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Comment');
            return $stmt->fetchAll();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve all comments!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function create() {
        $pdo = $this->getPdo();
        
        try {
            
            $sql = 'INSERT INTO comments SET
            name = :name,
            body = :body,
            entry_id = :entry_id';
            
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ':name' => $this->name,
                ':body' => $this->body,
                ':entry_id' => $this->entry_id,
            ]);
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to add new comment!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    
    public function getName() {
        return $this->name;
    }
    
    public function getBody() {
        return $this->body;
    }
    
}