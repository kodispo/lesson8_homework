<?php
class HtmlEntryWriter {
    
    static public function writeTable($entries) {
        $html = '';
        ob_start(); ?>
        
        <h1 class="h4 mb-3 text-uppercase">Entries</h1>
        <div class="table-responsive mb-4">
            <table class="table mb-0">
                <thead>
                <th>ID</th>
                <th>Title</th>
                <th></th>
                </thead>
                <tbody>
                <?php if (!empty($entries)) : ?>
                    <?php foreach ($entries as $entry) : ?>
                        <tr>
                            <td><?= $entry->getId(); ?></td>
                            <td><?= $entry->getTitle(); ?></td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <a href="/entry/details.php?id=<?= $entry->getId(); ?>" class="btn btn-outline-primary btn-sm">Details</a>
                                    <a href="/entry/edit.php?id=<?= $entry->getId(); ?>" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                    <form action="/entry/delete.php" method="post">
                                        <input type="hidden" name="id" value="<?= $entry->getId(); ?>">
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6">
                            <span class="text-danger">No entries yet.</span>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="text-right">
                        <a href="/entry/new.php" class="btn btn-outline-primary btn-sm">Add entry</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeCreateForm() {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase">Add new entry</h1>
        <form action="/entry/create.php" method="post">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">Title<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="title" name="title" placeholder="Enter title" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="intro" class="col-md-2 col-form-label">Intro</label>
                <div class="col-md-10">
                    <input type="text" id="intro" name="intro" placeholder="Enter intro" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">Content</label>
                <div class="col-md-10">
                    <textarea id="content" name="content" rows="4" placeholder="Enter content" class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Add entry</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeDetails(Entry $entry) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase text-center"><?= $entry->getTitle(); ?></h1>
        <h2 class="h5 mb-4 text-center"><?= $entry->getIntro(); ?></h2>
        <p><?= $entry->getContent(); ?></p>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeUpdateForm(Entry $entry) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase">Update entry</h1>
        <form action="/entry/update.php" method="post">
            <input type="hidden" name="id" value="<?= $entry->getId(); ?>">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">Title<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="title" name="title" value="<?= $entry->getTitle(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="intro" class="col-md-2 col-form-label">Intro</label>
                <div class="col-md-10">
                    <input type="text" id="intro" name="intro" value="<?= $entry->getIntro(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">Content</label>
                <div class="col-md-10">
                    <textarea id="content" name="content" rows="4" class="form-control"><?= $entry->getContent(); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Update entry</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
}