<?php
class Core {
    
    protected $pdo;
    
    public function __construct() {
        $host = 'localhost';
        $dbuser = 'root';
        $password = 'root';
        $dbname = 'lesson8_homework';
    
        try {
    
            $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $dbuser, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            $this->setPdo($pdo);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to connect to database!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    protected function getPdo() {
        return $this->pdo;
    }
    
    protected function setPdo(PDO $pdo) {
        $this->pdo = $pdo;
    }
    
}