<?php
class Entry extends Core {
    
    protected $id;
    protected $title;
    protected $intro;
    protected $content;
    
    public function __construct($title = '', $intro = '', $content = '') {
        parent::__construct();
        $this->title = htmlspecialchars($title);
        $this->intro = htmlspecialchars($intro);
        $this->content = htmlspecialchars($content);
    }
    
    public function getAll() {
        $pdo = $this->getPdo();
        
        try {
            
            $sql = 'SELECT id, title, intro, content FROM entries';
            $stmt = $pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Entry');
            return $stmt->fetchAll();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve all entries!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function create() {
        $pdo = $this->getPdo();
    
        try {
        
            $sql = 'INSERT INTO entries SET
            title = :title,
            intro = :intro,
            content = :content';
        
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ':title' => $this->title,
                ':intro' => $this->intro,
                ':content' => $this->content,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new entry!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $pdo = $this->getPdo();
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, title, intro, content
                FROM entries
                WHERE id = :id
            ';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Entry');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve entry information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        $pdo = $this->getPdo();
    
        try {
        
            $sql = 'UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id = :id';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':title' => $this->title,
                ':intro' => $this->intro,
                ':content' => $this->content,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update entry!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        $pdo = $this->getPdo();

        try {
    
            $sql = 'DELETE FROM comments WHERE entry_id = :id';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
            
            $sql = 'DELETE FROM entries WHERE id = :id';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to remove entry!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getIntro() {
        return $this->intro;
    }
    
    public function getContent() {
        return $this->content;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}