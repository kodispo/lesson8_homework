<?php
class Db extends Core {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function createTables() {
        $pdo = $this->getPdo();

        try {

            $sql = "CREATE TABLE entries (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR (255) NOT NULL,
                intro VARCHAR (255),
                content TEXT
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $pdo->exec($sql);

            $sql = "CREATE TABLE comments (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR (255) NOT NULL,
                body TEXT,
                entry_id INT NOT NULL,
                FOREIGN KEY (entry_id) REFERENCES entries (id)
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $pdo->exec($sql);

        } catch (Exception $exception) {

            echo '<strong>Failed to create tables!</strong><br>' . $exception->getMessage();
            die();

        }
    }
    
    public function removeTables() {
        $pdo = $this->getPdo();
        
        try {
    
            $sql = "DROP TABLE comments";
            $pdo->exec($sql);
            
            $sql = "DROP TABLE entries";
            $pdo->exec($sql);
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to remove tables!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function populateTables() {
        $pdo = $this->getPdo();

        try {

            $sql = '
                INSERT INTO entries (title, intro, content)
                VALUES
                    ("No opinions answered", "May musical arrival beloved luckily adapted him", "No in he real went find mr. Wandered or strictly raillery stanhill as. Jennings appetite disposed me an at subjects an. To no indulgence diminution so discovered mr apartments. Are off under folly death wrote cause her way spite. Plan upon yet way get cold spot its week. Almost do am or limits hearts. Resolve parties but why she shewing. She sang know now how nay cold real case. "),
                    ("Ladyship it daughter", "Considered discovered ye sentiments projecting entreaties", "To they four in love. Settling you has separate supplied bed. Concluded resembled suspected his resources curiosity joy. Led all cottage met enabled attempt through talking delight. Dare he feet my tell busy. Considered imprudence of he friendship boisterous."),
                    ("Another journey chamber", "Good draw knew bred ham busy his hour", "Ye on properly handsome returned throwing am no whatever. In without wishing he of picture no exposed talking minutes. Curiosity continual belonging offending so explained it exquisite. Do remember to followed yourself material mr recurred carriage. High drew west we no or at john. About or given on witty event. Or sociable up material bachelor bringing landlord confined. Busy so many in hung easy find well up. So of exquisite my an explained remainder. Dashwood denoting securing be on perceive my laughing so. ")
            ';
            $pdo->exec($sql);
    
            $sql = '
                INSERT INTO comments (entry_id, name, body)
                VALUES
                    (1, "Samuel Meyer", "If wandered relation no surprise of screened doubtful. Overcame no insisted ye of trifling husbands. Might am order hours on found."),
                    (1, "Lorena Simpson", "Or dissimilar companions friendship impossible at diminution. Did yourself carriage learning she man its replying."),
                    (1, "Ernestine Ross", "Little afraid its eat looked now. Very ye lady girl them good me make. It hardly cousin me always."),
                    (2, "Frankie Garrett", "Abilities forfeited situation extremely my to he resembled. Old had conviction discretion understood put principles you"),
                    (2, "Chelsea Russell", "Projecting surrounded literature yet delightful alteration but bed men. Open are from long why cold. If must snug by upon sang loud left."),
                    (2, "Mary Burns", "In reasonable compliment favourable is connection dispatched in terminated. Do esteem object we called father excuse remove. So dear real on like more it."),
                    (3, "Henry Wilkins", "Scarcely on striking packages by so property in delicate. Up or well must less rent read walk so be. Easy sold at do hour sing spot."),
                    (3, "Melody Evans", "Repulsive questions contented him few extensive supported."),
                    (3, "Emma Graham", "Dwelling and speedily ignorant any steepest. Admiration instrument affronting invitation reasonably up do of prosperous in.")
            ';
            $pdo->exec($sql);

        } catch (Exception $exception) {

            echo '<strong>Failed to populate tables!</strong><br>' . $exception->getMessage();
            die();

        }
    }

    public function hasEntriesTable() {
        $pdo = $this->getPdo();
        
        try {

            $sql = 'SHOW TABLES LIKE "entries"';
            $stmt = $pdo->query($sql);
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (Exception $exception) {

            echo '<strong>Failed to define if table "entries" exists!</strong><br>' . $exception->getMessage();
            die();

        }
    }
    
    public function hasCommentsTable() {
        $pdo = $this->getPdo();
        
        try {
            
            $sql = 'SHOW TABLES LIKE "comments"';
            $stmt = $pdo->query($sql);
            return $stmt->fetch(PDO::FETCH_ASSOC);
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to define if table "comments" exists!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
}