<?php
class HtmlCommentWriter {
    
    static public function writeList($comments) {
        $html = '';
        ob_start(); ?>
    
        <?php if (!empty($comments)) : ?>
            <p class="h5 mt-5">Comments</p>
            <ul class="list-group mb-5">
                <?php foreach ($comments as $comment) : ?>
                    <li class="list-group-item">
                        <h3 class="h5 mb-1">@<?= $comment->getName(); ?></h3>
                        <p class="mb-0"><?= $comment->getBody(); ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeCreateForm($entry_id) {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h5 mb-3 mt-5">Add new comment</h1>
        <form action="/comment/create.php" method="post">
            <input type="hidden" name="entry_id" value="<?= $entry_id; ?>">
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label">Name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="name" name="name" placeholder="Enter name" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="body" class="col-md-2 col-form-label">Body</label>
                <div class="col-md-10">
                    <textarea name="body" id="body" placeholder="Enter message" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Add comment</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
}