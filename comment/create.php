<?php
session_start();

if (empty($_POST['entry_id'])) {
    header('Location: /index.php');
    die();
}

if (empty($_POST['name'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to add new comment. Required fields must be specified'
    ];
    header('Location: /entry/details.php?id=' . $_POST['entry_id']);
    die();
}

require_once '../classes/Core.php';
require_once '../classes/Comment.php';

$comment = new Comment($_POST['name'], $_POST['body'], $_POST['entry_id']);
$comment->create();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'New comment has been added successfully'
];
header('Location: /entry/details.php?id=' . $_POST['entry_id']);