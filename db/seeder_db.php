<?php
session_start();
require_once '../classes/Core.php';
require_once '../classes/Db.php';

$db = new Db();
$db->populateTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables "entries" and "comments" have been populated successfully'
];
header('Location: /index.php');