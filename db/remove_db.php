<?php
session_start();
require_once '../classes/Core.php';
require_once '../classes/Db.php';

$db = new Db();
$db->removeTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables "entries" and "comments" have been removed successfully'
];
header('Location: /index.php');