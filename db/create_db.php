<?php
session_start();
require_once '../classes/Core.php';
require_once '../classes/Db.php';

$db = new Db();
$db->createTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables "entries" and "comments" have been created successfully'
];
header('Location: /index.php');